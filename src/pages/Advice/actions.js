export const fetchAdvice = (advice) => ({
  type: 'ADVICE_FETCHED',
  payload: advice,
});
