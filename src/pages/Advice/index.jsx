/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/media-has-caption */
import { Box, Card, Flex, Text } from '@chakra-ui/react';
import CasinoIcon from '@mui/icons-material/Casino';
import './index.css';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IconButton } from '@mui/material';
import { setTheme } from '@containers/App/actions';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import VolumeOffIcon from '@mui/icons-material/VolumeOff';
import backgroundMusic from '../../assets/snowy.mp3';
import backgroundMusicRain from '../../assets/rainy.mp3';

const Advice = () => {
  const dispatch = useDispatch();

  const theme = useSelector((state) => state.app.theme);

  const handleTheme = () => {
    dispatch(setTheme(theme === 'dark' ? 'light' : 'dark'));
  };

  useEffect(() => {
    dispatch({ type: 'LOAD_ADVICE', loading: true });
  }, [dispatch]);

  const advice = useSelector((store) => store.advicePayload);
  const loading = useSelector((store) => store.loading);

  const adviceClickHandler = (event) => {
    event.preventDefault();
    dispatch(dispatch({ type: 'LOAD_ADVICE', loading: true }));
  };

  const [isAudioPlaying, setIsAudioPlaying] = useState(false);

  const handlePlayButtonClick = () => {
    setIsAudioPlaying(true);
  };

  const handleStopButtonClick = () => {
    setIsAudioPlaying(false);
  };

  const audioSource = theme === 'dark' ? backgroundMusic : backgroundMusicRain;
  useEffect(() => {
    const audioElement = document.getElementById('background-music');
    if (audioElement) {
      audioElement.src = audioSource;
      if (isAudioPlaying) {
        audioElement.play();
      }
    }
  }, [theme]);

  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      style={{
        background: theme === 'dark' ? 'var(--bg-color)' : 'var(--card-bg)',
        backgroundSize: 'cover',
      }}
      height="100vh"
      width="100%"
    >
      <Card
        className={`theme-card ${theme === 'dark' ? 'dark-theme' : ''}`}
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        padding="4"
        overflow="visible"
        borderRadius="12px"
        maxwidth="520px"
        style={{
          background: theme === 'dark' ? 'var(--card-bg)' : 'var(--card-color)',
          backgroundSize: 'cover',
        }}
      >
        <IconButton
          sx={{
            position: 'absolute',
            top: '10px',
            right: '10px',
            color: theme === 'dark' ? 'var(--accent-color)' : 'var(--card-color)',
          }}
          onClick={handleTheme}
        >
          {theme === 'dark' ? <Brightness4Icon /> : <Brightness7Icon />}
        </IconButton>
        {!isAudioPlaying && (
          <IconButton
            sx={{
              position: 'absolute',
              top: '10px',
              left: '10px',
            }}
            onClick={handlePlayButtonClick}
          >
            <VolumeOffIcon />
          </IconButton>
        )}
        {isAudioPlaying && (
          <div>
            <IconButton
              sx={{
                position: 'absolute',
                top: '10px',
                left: '10px',
              }}
              onClick={handleStopButtonClick}
            >
              <VolumeUpIcon />
            </IconButton>
            <audio id="background-music" autoPlay loop>
              <source src={audioSource} type="audio/mpeg" />
              Your browser does not support the audio element.
            </audio>
          </div>
        )}
        <Text
          fontSize="1rem"
          color={theme === 'dark' ? 'var(--accent-color)' : 'var(--accent-color)'}
          m="3"
          mt="40px"
          fontWeight="500"
          fontFamily="Manrope"
          letterSpacing="4px"
        >
          ADVICE #{advice.id}
        </Text>
        {loading ? (
          <i
            className="fas fa-circle-notch fa-spin"
            style={{ fontSize: '60px', color: 'hsl(193, 38%, 86%)', marginTop: '30px' }}
          />
        ) : (
          <Text
            fontFamily="Manrope"
            color="hsl(193, 38%, 86%)"
            textAlign="center"
            fontSize="30px"
            fontWeight="750"
            mb="4"
            mr="5%"
            ml="5%"
            maxWidth="400px"
            className="waviy"
          >
            <span>&ldquo; {advice.advice} &rdquo;</span>
          </Text>
        )}
        <div className="svg">
          <svg width="444" height="16" xmlns="http://www.w3.org/2000/svg">
            <g fill="none" fillRule="evenodd">
              <path fill="#4F5D74" d="M0 8h196v1H0zM248 8h196v1H248z" />
              <g transform="translate(212)" fill="#CEE3E9">
                <rect width="6" height="16" rx="3" />
                <rect x="14" width="6" height="16" rx="3" />
              </g>
            </g>
          </svg>
        </div>
        <Box
          position="relative"
          bottom="-40px"
          backgroundColor="hsl(150, 100%, 66%)"
          borderRadius="50%"
          p="10px"
          pt="18px"
          opacity=".7"
          cursor="pointer"
          scale="1.05"
          onClick={!loading ? adviceClickHandler : ''}
        >
          <CasinoIcon className={`randomizer ${loading ? 'spin' : ''}`} sx={{ fontSize: '2rem' }} />
        </Box>
      </Card>
    </Flex>
  );
};

export default Advice;
