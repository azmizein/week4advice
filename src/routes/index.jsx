import MainLayout from '@layouts/MainLayout';

// import Home from '@pages/Home';
import NotFound from '@pages/NotFound';
import Advice from '@pages/Advice';

const routes = [
  {
    path: '/',
    name: 'Advice',
    component: Advice,
    // layout: MainLayout,
  },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
