import { all } from 'redux-saga/effects';

// import appSaga from '@containers/App/saga';
import Advice from '@pages/Advice/saga';

export default function* rootSaga() {
  yield all([Advice()]);
}
