import { combineReducers } from 'redux';

import appReducer, { storedKey as storedAppState } from '@containers/App/reducer';
import languageReducer from '@containers/Language/reducer';

import { fetchAdviceReducer, loadingAdviceReducer } from '@pages/Advice/reducers';
import { mapWithPersistor } from './persistence';

const storedReducers = {
  app: { reducer: appReducer, whitelist: storedAppState },
};

const temporaryReducers = {
  language: languageReducer,
  loading: loadingAdviceReducer,
  advicePayload: fetchAdviceReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
